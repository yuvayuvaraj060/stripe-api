import { stripe } from ".";

export const createPaymentIntend = async (amount: number) => {
  const paymentIntent = await stripe.paymentIntents.create({
    amount,
    currency: "inr",
  });
  console.log(
    "🚀 ~ file: paymentIntents.ts ~ line 8 ~ createPaymentIntend ~ paymentIntent",
    paymentIntent
  );

  return paymentIntent;
};
