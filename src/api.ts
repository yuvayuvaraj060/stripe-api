import express, { NextFunction, Request, Response } from "express";
import cors from "cors";
import { createStripeCheckoutSession } from "./checkOut";
import { createPaymentIntend } from "./paymentIntents";

export const app = express();

// middleware
app.use(express.json());
app.use(express.urlencoded());
app.use(cors({ origin: true }));
app.use(express.static("."));

app.get("/", (req, res) => {
  console.log("🚀 ~ file: api.ts ~ line 7 ~ app.get ~ req", req);

  res.send("<h1>Working In root url</h1>");
});

app.post(
  "/checkout",
  runAsync(async ({ body }: Request, res: Response) => {
    console.log("🚀 ~ file: api.ts ~ line 13 ~ runAsync ~ body", body);

    res.send(await createStripeCheckoutSession(body.line_items));
  })
);

app.post(
  "/payment",
  runAsync(async ({ body }: Request, res: Response) => {
    console.log("🚀 ~ file: api.ts ~ line 32 ~ runAsync ~ body", body);
    res.send(await createPaymentIntend(body.amount));
  })
);

function runAsync(callback: Function) {
  return (req: Request, res: Response, next: NextFunction) => {
    callback(req, res, next).catch(next);
  };
}
