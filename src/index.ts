// env variables
import { config } from "dotenv";
import { app } from "./api";
import Stripe from "stripe";


if (process.env.NODE_ENV !== "production") {
  config();
}

// Initialize Stripe
export const stripe = new Stripe(process.env.STRIP_SECRET, {
  apiVersion: "2020-08-27",
});

// listen
const port = process.env.PORT || 3333;
app.listen(port, () =>
  console.log(`API is available on http://localhost:${port}`)
);
