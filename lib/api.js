"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const checkOut_1 = require("./checkOut");
const paymentIntents_1 = require("./paymentIntents");
exports.app = express_1.default();
// middleware
exports.app.use(express_1.default.json());
exports.app.use(express_1.default.urlencoded());
exports.app.use(cors_1.default({ origin: true }));
exports.app.use(express_1.default.static("."));
exports.app.get("/", (req, res) => {
    console.log("🚀 ~ file: api.ts ~ line 7 ~ app.get ~ req", req);
    res.send("<h1>Working In root url</h1>");
});
exports.app.post("/checkout", runAsync(async ({ body }, res) => {
    console.log("🚀 ~ file: api.ts ~ line 13 ~ runAsync ~ body", body);
    res.send(await checkOut_1.createStripeCheckoutSession(body.line_items));
}));
exports.app.post("/payment", runAsync(async ({ body }, res) => {
    console.log("🚀 ~ file: api.ts ~ line 32 ~ runAsync ~ body", body);
    res.send(await paymentIntents_1.createPaymentIntend(body.amount));
}));
function runAsync(callback) {
    return (req, res, next) => {
        callback(req, res, next).catch(next);
    };
}
//# sourceMappingURL=api.js.map