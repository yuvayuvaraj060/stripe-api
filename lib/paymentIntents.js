"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createPaymentIntend = void 0;
const _1 = require(".");
const createPaymentIntend = async (amount) => {
    const paymentIntent = await _1.stripe.paymentIntents.create({
        amount,
        currency: "inr",
    });
    console.log("🚀 ~ file: paymentIntents.ts ~ line 8 ~ createPaymentIntend ~ paymentIntent", paymentIntent);
    return paymentIntent;
};
exports.createPaymentIntend = createPaymentIntend;
//# sourceMappingURL=paymentIntents.js.map