"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStripeCheckoutSession = void 0;
const _1 = require("./");
const createStripeCheckoutSession = async (line_items) => {
    console.log("🚀 ~ file: checkOut.ts ~ line 7 ~ line_items", line_items);
    // get web app url
    const url = process.env.WEB_APP_URL;
    const session = await _1.stripe.checkout.sessions.create({
        payment_method_types: ["card"],
        line_items,
        success_url: `${url}/success?session_id={CHECKOUT_SESSION_ID}`,
        cancel_url: `${url}/failed`,
    });
    return { id: session.id };
};
exports.createStripeCheckoutSession = createStripeCheckoutSession;
//# sourceMappingURL=checkOut.js.map