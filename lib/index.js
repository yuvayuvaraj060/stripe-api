"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.stripe = void 0;
// env variables
const dotenv_1 = require("dotenv");
const api_1 = require("./api");
const stripe_1 = __importDefault(require("stripe"));
if (process.env.NODE_ENV !== "production") {
    dotenv_1.config();
}
// Initialize Stripe
exports.stripe = new stripe_1.default(process.env.STRIP_SECRET, {
    apiVersion: "2020-08-27",
});
// listen
const port = process.env.PORT || 3333;
api_1.app.listen(port, () => console.log(`API is available on http://localhost:${port}`));
//# sourceMappingURL=index.js.map